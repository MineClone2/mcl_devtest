math.randomseed(os.time())

local function get_point_on_circle(pos,r,n)
	local rt = {}
	for i=1, n do
		table.insert(rt,vector.offset(pos,r * math.cos(((i-1)/n) * (2*math.pi)),0,  r* math.sin(((i-1)/n) * (2*math.pi)) ))
	end
	return rt[math.random(#rt)]
end

minetest.register_chatcommand("mobtest",{
	description="Spawns all available mobs",
	privs={server=true},
	func=function(n,param)
	local p=minetest.get_player_by_name(n)
	local pos=p:get_pos()
	for k,v in pairs(minetest.registered_entities) do
		if v.is_mob then
			local spos=get_point_on_circle(pos,28,32)
			local o = mcl_mobs.spawn(spos,v.name)
			if o then
				local l=o:get_luaentity()
				if l and l.on_activate then
					l.on_activate(l)
				end
			end

		end
	end
end})

local function make_bubble(pos)
		minetest.set_node(vector.offset(pos,0,-1,0),{name="mcl_core:obsidian"})
		minetest.set_node(vector.offset(pos,1,0,0),{name="mcl_core:glass"})
		minetest.set_node(vector.offset(pos,-1,0,0),{name="mcl_core:glass"})
		minetest.set_node(vector.offset(pos,0,0,1),{name="mcl_core:glass"})
		minetest.set_node(vector.offset(pos,0,0,-1),{name="mcl_core:glass"})
		minetest.set_node(vector.offset(pos,1,1,0),{name="mcl_core:glass"})
		minetest.set_node(vector.offset(pos,-1,1,0),{name="mcl_core:glass"})
		minetest.set_node(vector.offset(pos,0,1,1),{name="mcl_core:glass"})
		minetest.set_node(vector.offset(pos,0,1,-1),{name="mcl_core:glass"})
		minetest.set_node(vector.offset(pos,0,2,0),{name="mcl_core:glass"})
end

local function make_circle(pos,n1,n2,light)
		local p1 = vector.offset(pos,-36,-5,-36)
		local p2 = vector.offset(pos,36,5,36)
		local nn1 = minetest.find_nodes_in_area_under_air(p1,p2,{"group:solid","group:plant","group:leaves","group:tree","group:mushroom"})
		local c1 = {}
		local c2 = {}
		for k,v in pairs(nn1) do
			local dst1=vector.distance(vector.round(vector.new(pos.x,0,pos.z)),vector.round(vector.new(v.x,0,v.z)))
			local dst2=vector.distance(vector.round(vector.new(pos.x,0,pos.z)),vector.round(vector.new(v.x,0,v.z)))
			if  dst1 > 20 and dst1 < 22 then
				table.insert(c1,v)
			elseif  dst2 > 33 and dst2 < 35 then
				table.insert(c2,v)
			end
		end
		local cobble = {}
		local brick = {}
		for k,v in pairs(c1) do
			for i=1,6 do table.insert(cobble,vector.offset(v,0,i,0)) end
			minetest.set_node(vector.offset(v,0,6,0),{name="mcl_stairs:slab_andesite_smooth"})
		end
		for k,v in pairs(c2) do
			for i=1,8 do table.insert(brick,vector.offset(v,0,i,0)) end
			minetest.set_node(vector.offset(v,0,8,0),{name="mcl_stairs:slab_diorite_smooth"})
		end
		if light then minetest.bulk_set_node(c1,{name="mcl_nether:glowstone"}) end
		minetest.bulk_set_node(cobble,{name=n1})
		minetest.bulk_set_node(brick,{name=n2})
end

mcl_structures.register_structure("spawncircle",{
	place_func = function(pos,def,pr)
		make_circle(pos,"mcl_core:glass","mcl_core:stonebrick")
		make_bubble(pos)
	end
},true)

mcl_structures.register_structure("spawncircle_lit",{
	place_func = function(pos,def,pr)
		make_circle(pos,"mcl_core:glass","mcl_nether:glowstone",true)
		make_bubble(pos)
	end
},true)
