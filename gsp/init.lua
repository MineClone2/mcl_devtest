local times={}
local max_times = {}
local total=0
local ngs=0
local function roundN(n, d)
	if type(n) ~= "number" then return n end
    local m = 10^d
    return math.floor(n * m + 0.5) / m
end

local function get_out(inp)
	local out = {}
	for k,v in pairs(inp) do
		out[k] = v
	end
	return out
end

minetest.register_on_mods_loaded(function()
	for i, gs in ipairs(minetest.registered_globalsteps) do
		local info = minetest.callback_origins[gs]
		local n=info.mod
		max_times[n] = 0
		minetest.registered_globalsteps[i]=function(dtime)
			local t1=os.clock()
			local r=gs(dtime)
			local t2=os.clock()
			times[n]=roundN(os.clock()-t1,12)
			if times[n] > max_times[n] then max_times[n] = times[n] end
			total=total+times[n]
			return r
		end
	end
	minetest.register_globalstep(function(dtime)
		--minetest.log(tostring(dtime))
		minetest.after(0, function()
			if dtime > 0.5 then
				local d = get_out(times)
				local m = get_out(max_times)
				table.sort(d,function(a,b)
					return a > b
				end)
				table.sort(m,function(a,b)
					return a > b
				end)
				--minetest.log("LONGSTEP: "..dtime)
				--minetest.log("times:"..dump(d))
				--minetest.log("LONGSTEP")
			end
		end)
	end)
end)


minetest.register_chatcommand("gsp",{privs={debug=true},
	description="Give an overview of the registered globalsteps and how long they each take",
	func=function(p)
	table.sort(times)
	minetest.chat_send_player(p,"=== times: \n"..dump(times))
	minetest.chat_send_player(p,"=== maxtimes: \n"..dump(max_times))
	minetest.chat_send_player(p,"total globalstep time last interval:"..dump(total))
	minetest.chat_send_player(p,"total globalsteps registered:"..dump(ngs))
end})
