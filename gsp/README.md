# gsp - globalstep profiler

Dumps a list of the globalsteps and how long they each took last round.

## Chatcommands

/gsp - dump report
