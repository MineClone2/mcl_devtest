minetest.register_chatcommand("generate_decos",{
	privs = {debug = true},
	func = function(name,param)
		local w = 25
		local p = minetest.get_player_by_name(name):get_pos()
		local p1 = vector.offset(p,-w,-w,-w)
		local p2 = vector.offset(p,w,w,w)
		local vm = minetest.get_voxel_manip(p1, p2)
		minetest.generate_decorations(vm,p1,p2)
	end,
})
