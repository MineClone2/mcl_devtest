local length = 50

local function generate_gallery(pos,place_func)
	local i=1
	for n,node in pairs(minetest.registered_nodes) do
		place_func(vector.add(pos,vector.new(i % length,math.ceil(i / length),0)),{name=n})
		i = i + 1
	end
end
local nodenames={}
local itemnames={}
minetest.register_on_mods_loaded(function()
	for n,node in pairs(minetest.registered_nodes) do
		if not n:find("meshhand") then
			table.insert(nodenames,n)
		end
	end
	for n,node in pairs(minetest.registered_items) do
		if not n:find("meshhand") then
			table.insert(itemnames,n)
		end
	end
end)
local function generate_gallery_cube(pos)
	local i=1
	local j=1
	local x=1
	local y=1
	local z=1
	local length=10
	for n,node in pairs(minetest.registered_nodes) do
		minetest.set_node(vector.add(pos,vector.new(x,y,z)),{name=n})
		if x > length then
			x = 1
			z = z + 1
		else
			x = x + 1
		end
		if z > length then
			y = y + 1
			z = 1
		end
	end
end

minetest.register_chatcommand("gallery",{
	description="Generates a wall of all registered nodes next to the player",
	privs={debug=true},
	func=function(name,p)
	local pl=minetest.get_player_by_name(name)
	generate_gallery(vector.add(pl:get_pos(),vector.new(0,0,5)),function(pos,node)
		minetest.set_node(pos,node)
	end)
end})

minetest.register_chatcommand("gallerycube",{
	description="Generates a cube of all registered nodes next to the player",
	privs={debug=true},
	func=function(name,p)
	local pl=minetest.get_player_by_name(name)
	generate_gallery_cube(vector.add(pl:get_pos(),vector.new(0,0,5)))
end})

math.randomseed(os.clock())

local a = 10

minetest.register_chatcommand("test_signs",{
	description="Generates a wall of signs",
	privs={debug=true},
	func=function(name,p)
	local pl=minetest.get_player_by_name(name)
	local pos = pl:get_pos()
	if p and tonumber(p) then a = p end
	local i=1
	for i=1,a do
		local fp = vector.add(pos,vector.new(i % length,math.ceil(i / length),0))
		minetest.set_node(fp,{name="mcl_signs:standing_sign_warped_hyphae_wood"})
		local m = minetest.get_meta(fp)
		m:set_string("text","test\n"..fp.x..fp.y..fp.z)
		m:set_string("mcl_signs:text_color","#00FF00")
		i = i + 1
	end
end})
local iframe_def = minetest.registered_nodes["mcl_itemframes:item_frame"]

minetest.register_chatcommand("test_iframes",{
	description="Generates a wall itemframes",
	privs={debug=true},
	func=function(name,p)
	local pl=minetest.get_player_by_name(name)
	local pos = pl:get_pos()
	if p and tonumber(p) then a = p end
	local i=1
	for i=1,a do
		local l = 20
		local fp = vector.add(pos,vector.new(i % l,math.ceil(i / l),0))
		minetest.set_node(fp,{name="mcl_itemframes:item_frame"})
		local m = minetest.get_meta(fp)
		local inv = m:get_inventory()
		local n = itemnames[math.random(#itemnames)]
		local nn = minetest.get_node(fp)
		inv:set_stack("main",1,ItemStack(n))
		m:set_string("roll",math.random(0,7))
		if iframe_def.on_construct then iframe_def.on_construct(p) end
		--mcl_itemframes.update_item_entity(fp, nn,nn.param2)
		i = i + 1
	end
end})

minetest.register_chatcommand("iframes_gallery",{
	description="Generates a wall of itemframes of all nodes",
	privs={debug=true},
	func=function(name,p)
	local pl=minetest.get_player_by_name(name)
	local pos = pl:get_pos()
	generate_gallery(vector.add(pl:get_pos(),vector.new(0,0,5)),function(p,node)
		minetest.set_node(p,{name="mcl_itemframes:item_frame"})
		local m = minetest.get_meta(p)
		local inv = m:get_inventory(p)
		inv:set_stack("main",1,ItemStack(itemnames[math.random(#itemnames)]))
		m:set_string("roll",math.random(0,7))
		if iframe_def.on_construct then iframe_def.on_construct(p) end
	end)
end})
