local current = 0
local biomekeys = {}
local wait = 15
local old_damage = minetest.settings:get_bool("enable_damage")
local stay_near = nil
active = false
minetest.register_on_mods_loaded(function()
	for k,_ in pairs(minetest.registered_biomes) do
		table.insert(biomekeys,k)
	end
	table.shuffle(biomekeys)
end)

local function next_biome()
	current = current + 1
	if biomekeys[current] and minetest.registered_biomes[biomekeys[current]] then
		return minetest.registered_biomes[biomekeys[current]].name
	end
	current = 0
	active = false
end

local function tp_biome(n)
	local b = next_biome()
	if not b then
		minetest.chat_send_player(n,"Done! "..tostring(#biomekeys).." biomes visited.")
		return
	end
	if stay_near then
		minetest.get_player_by_name(n):set_pos(stay_near)
	end
	minetest.registered_chatcommands["findbiome"].func(n,b)
	local name = minetest.registered_biomes[biomekeys[current]].name
	minetest.chat_send_player(n,"("..current.."/"..#biomekeys.."): "..name)
	--minetest.log("action","[mcl_structures] BIOME: "..name)
	if active then minetest.after(wait,tp_biome,n) end
end

minetest.register_chatcommand("biometp",{
	privs = {debug = true},
	description = "Teleports to all biomes successively.",
	params = "|<tp interval>|<stay>|<roam>",
	func = function(n,p)
		local pn = tonumber(p)
		if pn and pn > 0 then
			wait = pn
			return true,"Biometp interval set to "..pn
		end
		if p == "stay" then
			stay_near = vector.round(minetest.get_player_by_name(n):get_pos())
			return true,"Staying around "..minetest.pos_to_string(stay_near)
		end
		if p == "roam" then
			stay_near = nil
			return true,"Roaming around freely"
		end
		if not active then
			active = true
			old_damage = minetest.settings:get_bool("enable_damage")
			minetest.settings:set_bool("enable_damage",false)
			minetest.after(wait,tp_biome,n)
			if stay_near then minetest.get_player_by_name(n):set_pos(stay_near) end
			return true,"Biometp started ETA: "..tostring(math.ceil(#biomekeys * wait / 60)) .."mins"
		end
		active = false
		minetest.settings:set_bool("enable_damage",old_damage)
		return true,"Biometp stopped"
	end
})
