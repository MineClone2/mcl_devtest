math.randomseed(os.clock())
minetest.register_node("unknower:unknown"..tostring(math.random(65000)), {
	description = "Unknownmaker",
	_doc_items_longdesc = "When placed node will become unknown on next server restart.",
	tiles = {"mcl_core_diorite_smooth.png"},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=1, stone=1, building_block=1, material_stone=1},
	_mcl_blast_resistance = 6,
	_mcl_hardness = 1.5,
})
